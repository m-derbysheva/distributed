package ru.nsu.fit.mderbysheva.lab;

import org.jgroups.JChannel;
import org.jgroups.ReceiverAdapter;
import org.jgroups.View;
import org.jgroups.blocks.RequestOptions;
import org.jgroups.blocks.RpcDispatcher;
import org.jgroups.blocks.locking.LockService;
import org.jgroups.util.Rsp;
import org.jgroups.util.RspList;
import org.jgroups.util.Util;

import java.io.*;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.locks.Lock;

/**
 * Replicated stock server; every cluster node has the same state (stocks).
 */
public class ReplicatedStockServer extends ReceiverAdapter {
    private final Map<String, Double> stocks = new HashMap<>();
    private final JChannel channel;
    private final LockService lockService;
    private final RpcDispatcher rpcDispatcher; // to invoke RPCs

    private ReplicatedStockServer(String properties) throws Exception {
        channel = new JChannel(properties);
        rpcDispatcher = new RpcDispatcher(channel, this).setMembershipListener(this);
        lockService = new LockService(channel);
        rpcDispatcher.setStateListener(this);
    }

    /**
     * Assigns a value to a stock
     */
    public void _setStock(String name, Double value) {
        synchronized (stocks) {
            stocks.put(name, value);
            System.out.printf("-- set %s to %s\n", name, value);
        }
    }

    /**
     * Removes a stock from the HashMap
     */
    public void _removeStock(String name) {
        synchronized (stocks) {
            stocks.remove(name);
            System.out.printf("-- removed %s\n", name);
        }
    }

    /**
     * Get a stock from the HashMap
     */
    public Double _getStock(String name) {
        synchronized (stocks) {
            Double val = stocks.get(name);
            System.out.printf("-- returned %s with %s\n", name, val);
            return val;
        }
    }

    /**
     * Set a stock if it's value equal to ref
     */
    public boolean _casStock(String name, double reference, Double newValue) {
        synchronized (stocks) {
            Double val = stocks.get(name);
            if (val == reference) {
                stocks.put(name, newValue);
                System.out.printf("-- cas -- set %s to %s\n", name, newValue);
                return true;
            } else {
                System.out.printf("-- cas -- failed in setting %s to %s\n", name, newValue);
                return false;
            }
        }
    }

    private void start() throws Exception {
        channel.connect("stocks");
        rpcDispatcher.start();
        channel.getState(null, 30000); // fetches the state from the coordinator
        while (true) {
            int c = Util.keyPress("[1] Show stocks [2] Get quote [3] Set quote [4] Remove quote [5] CAS [x] Exit");
            try {
                switch (c) {
                    case '1':
                        showStocks();
                        break;
                    case '2':
                        getStock();
                        break;
                    case '3':
                        setStock();
                        break;
                    case '4':
                        removeStock();
                        break;
                    case '5':
                        casStock();
                        break;
                    case 'x':
                        channel.close();
                        return;
                }
            } catch (Exception ex) {
                System.out.println(ex.toString());
            }
        }
    }


    public void viewAccepted(View view) {
        System.out.println("-- VIEW: " + view);
    }

    @Override
    public void getState(OutputStream output) throws Exception {
        DataOutput out = new DataOutputStream(output);
        synchronized (stocks) {
            System.out.println("-- returning " + stocks.size() + " stocks");
            Util.objectToStream(stocks, out);
        }
    }


    @Override
    public void setState(InputStream input) throws Exception {
        DataInput in = new DataInputStream(input);
        Map<String, Double> newState = Util.objectFromStream(in);
        System.out.println("-- received state: " + newState.size() + " stocks");
        synchronized (stocks) {
            stocks.clear();
            stocks.putAll(newState);
        }
    }


    private void getStock() throws IOException {
        String ticker = readString("Symbol");
        Lock lock = lockService.getLock("lock" + ticker);
        lock.lock();
        try {
            synchronized (stocks) {
                Double val = stocks.get(ticker);
                System.out.println(ticker + " is " + val);
            }
        } finally {
            lock.unlock();
        }
    }

    private void setStock() throws Exception {
        String ticker, val;
        ticker = readString("Symbol");
        val = readString("Value");
        Lock lock = lockService.getLock("lock" + ticker);
        lock.lock();
        try {
            RspList<Void> rspList = rpcDispatcher.callRemoteMethods(null, "_setStock", new Object[]{ticker, Double.parseDouble(val)},
                    new Class[]{String.class, Double.class}, RequestOptions.SYNC());
            System.out.println("rspList:\n" + rspList);
        } finally {
            lock.unlock();
        }
    }


    private void removeStock() throws Exception {
        String ticker = readString("Symbol");
        Lock lock = lockService.getLock("lock" + ticker);
        lock.lock();
        try {
            RspList<Void> rspList = rpcDispatcher.callRemoteMethods(null, "_removeStock", new Object[]{ticker},
                    new Class[]{String.class}, RequestOptions.SYNC());
            System.out.println("rspList:\n" + rspList);
        } finally {
            lock.unlock();
        }
    }

    private void casStock() throws Exception {
        String ticker = readString("Symbol");
        double oldValue = Double.parseDouble(readString("Old value"));
        String val = readString("New value");

        Lock lock = lockService.getLock("lock" + ticker);
        lock.lock();
        try {
            System.out.printf("Old value: %s", oldValue);

            RspList<Boolean> rspList = rpcDispatcher.callRemoteMethods(null, "_casStock",
                    new Object[]{ticker, oldValue, (Double.parseDouble(val))},
                    new Class[]{String.class, double.class, Double.class}, RequestOptions.SYNC());

            int countSucceeded = 0;

            for (Rsp<Boolean> v : rspList) {
                if (v.wasReceived() && v.getValue()) {
                    countSucceeded++;
                }
            }

            boolean succeeded = countSucceeded > rspList.numReceived() / 2;

            System.out.println("cas: " + ticker + " set to " + val + (succeeded ? " successfully" : " FAILED"));
        } finally {
            lock.unlock();
        }
    }


    private void showStocks() {
        System.out.println("Stocks:");
        synchronized (stocks) {
            for (Map.Entry<String, Double> entry : stocks.entrySet()) {
                System.out.println(entry.getKey() + ": " + entry.getValue());
            }
        }
    }


    private static String readString(String s) throws IOException {
        int c;
        boolean looping = true;
        StringBuilder sb = new StringBuilder();
        System.out.print(s + ": ");
        System.out.flush();
        long available = System.in.available();
        long skipped = System.in.skip(available);
        if (available != skipped) {
            System.err.println("available(" + available + ") != skipped(" + skipped + ")");
        }

        while (looping) {
            c = System.in.read();
            switch (c) {
                case -1:
                case '\n':
                case 13:
                    looping = false;
                    break;
                default:
                    sb.append((char) c);
                    break;
            }
        }

        return sb.toString();
    }


    public static void main(String[] args) throws Exception {
        String props = "udp.xml";
        for (int i = 0; i < args.length; i++) {
            if (args[i].equals("-props")) {
                props = args[++i];
                continue;
            }
            System.out.println("ReplicatedStockServer [-props <XML config file>]");
            return;
        }

        new ReplicatedStockServer(props).start();
    }


}
